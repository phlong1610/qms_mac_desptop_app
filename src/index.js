const { app, BrowserWindow, ipcMain, Notification } = require('electron')
const path = require('path')
require('electron-reload')(__dirname)
require('dotenv').config()

let mainWindow
let workerWindow
function createWindow() {
  makeSingleInstance()
  // Create the browser window.
  mainWindow = new BrowserWindow({
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true,
      worldSafeExecuteJavaScript: true
    }
  })

  // and load the index.html of the app.
  mainWindow.loadURL(`file://${__dirname}/getNum/index.html`)
  // mainWindow.loadURL('https://qmstest.digihcs.com/')

  // Open the DevTools.
  mainWindow.webContents.setWebRTCIPHandlingPolicy('default_public_and_private_interfaces')
  // mainWindow.setKiosk(true)
  mainWindow.on('closed', function () {
    mainWindow = null
  })
  // workerWindow = new BrowserWindow({
  //   webPreferences: {
  //     nodeIntegration: true,
  //     worldSafeExecuteJavaScript: true,
  //     // preload: __dirname + '/preload.js'
  //   }
  // });
  // workerWindow.loadURL(`file://${__dirname}/getNum/worker.html`)
  // workerWindow.hide()
  mainWindow.webContents.openDevTools();
  // workerWindow.on('closed', () => {
  //   workerWindow = null
  // })
}

ipcMain.on('notification', (event, args) => {
  console.log('Notification support: ' + Notification.isSupported());
  const notification = {
    title: 'Basic Notification',
    body: 'Short message part'
  }
  const myNotification = new window.Notification(notification.title, notification)

  myNotification.onclick = () => {
    console.log('Notification clicked')
  }
})
// ipcMain.on('print', (event, content) => {
//   if (workerWindow) {
//     workerWindow.webContents.send('print', content)
//   }
// })
// ipcMain.on('readyToPrint', (event) => {
//   if (workerWindow) {
//     workerWindow.webContents.print({
//       silent: false,
//       printBackground: true
//     })
//   }
// })

app.whenReady().then(() => {
  createWindow()

  app.on('activate', function () {
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

const makeSingleInstance = () => {

  if (process.mas) return

  app.requestSingleInstanceLock()

  app.on('second-instance', () => {
    if (mainWindow) {
      if (mainWindow.isMinimized()) mainWindow.restore()
      mainWindow.focus()
    }
  })
}